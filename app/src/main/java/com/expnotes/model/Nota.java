package com.expnotes.model;

import android.location.Location;



/**
 * Created by jmimora on 19/11/16.
 */

public abstract class Nota {
    public static final int NOTA_TEXT = 1;
    public static final int NOTA_IMATGE = 2;
    public static final int NOTA_AUDIO = 3;

    private String email;
    private String expedient;
    private String id;
    private String titol;
    private Location location;
    private long momentCreacio;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Nota(String email, String expedient) {
        this.email = email;
        this.expedient = expedient;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getExpedient() {
        return expedient;
    }

    public void setExpedient(String expedient) {
        this.expedient = expedient;
    }

    public long getMomentCreacio() {
        return momentCreacio;
    }

    public void setMomentCreacio(long momentCreacio) {
        this.momentCreacio = momentCreacio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Nota newNota(String email, String expedient, int tipusNota) {
        switch (tipusNota) {
            case NOTA_TEXT:
                return new NotaText(email, expedient);

            case NOTA_IMATGE:
                return new NotaImatge(email, expedient);

            case NOTA_AUDIO:
                return new NotaAudio(email, expedient);
        }

        return null;
    }

    public abstract int tipusNota();
    public abstract String getNomFitxer();
}
