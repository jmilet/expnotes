package com.expnotes.model;

import android.location.Location;

import com.expnotes.utils.DateTimeHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jmimora on 12/11/16.
 */

public class Expedient {

    public static final String ESTAT_OBERT = "O";
    public static final String ESTAT_ESPERA = "E";
    public static final String ESTAT_TANCAT = "T";

    public static final String LITERAL_ESTAT_OBERT = "Obert";
    public static final String LITERAL_ESTAT_ESPERA = "Espera";
    public static final String LITERAL_ESTAT_TANCAT = "Tancat";

    private static final Map<String, String> sLiteralsEstats;
    static
    {
        sLiteralsEstats = new HashMap<String, String>();
        sLiteralsEstats.put(ESTAT_OBERT, LITERAL_ESTAT_OBERT);
        sLiteralsEstats.put(ESTAT_ESPERA, LITERAL_ESTAT_ESPERA);
        sLiteralsEstats.put(ESTAT_TANCAT, LITERAL_ESTAT_TANCAT);
    }

    private String email;
    private String id;
    private String titol;
    private String descripcio;
    private String estat;
    private Location location;
    private int prioritat;
    private long momentCreacio;
    private long momentTancament;

    public Expedient(String email, String id) {
        this.email = email;
        this.id = id;
        this.titol = "Titol";
        this.descripcio = "Descripció";
        this.estat = ESTAT_OBERT;
        this.prioritat = 5;
        this.momentTancament = -1;
        this.momentCreacio = DateTimeHelper.nowUnixEpoch();
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public void setEstat(String estat) {
        switch (estat) {
            case ESTAT_ESPERA:
                en_espera();
                break;
            case ESTAT_OBERT:
                obert();
                break;
            case ESTAT_TANCAT:
                tancar();
                break;
        }
    }

    public String getEstat() {
        return estat;
    }

    public int getPrioritat() {
        return prioritat;
    }

    public void setPrioritat(int prioritat) {
        this.prioritat = prioritat;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public long getMomentCreacio() {
        return momentCreacio;
    }

    public void setMomentCreacio(long momentCreacio) {
        this.momentCreacio = momentCreacio;
    }

    public long getMomentTancament() {
        return momentTancament;
    }

    public void setMomentTancament(long momentTancament) {
        this.momentTancament = momentTancament;
    }

    public void tancar() {
        this.estat = ESTAT_TANCAT;
        this.momentTancament = DateTimeHelper.nowUnixEpoch();
    }

    public String getLiteralEstat() {
        return sLiteralsEstats.get(this.estat);
    }

    public void en_espera() {
        this.estat = ESTAT_ESPERA;
        this.momentTancament = -1;
    }

    public void obert() {
        this.estat = ESTAT_OBERT;
        this.momentTancament = -1;
    }

    @Override
    public String toString() {
        return this.email + "_" + this.id;
    }
}
