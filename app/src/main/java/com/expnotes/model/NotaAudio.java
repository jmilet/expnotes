package com.expnotes.model;

/**
 * Created by jmimora on 26/11/16.
 */

public class NotaAudio extends Nota {

    public NotaAudio(String email, String expedient) {
        super(email, expedient);
    }

    public String getNomFitxer() {
        return String.format("AUD_%s_%s.jpg", this.getExpedient(), this.getId());
    }

    public int tipusNota() {
        return Nota.NOTA_AUDIO;
    }

}