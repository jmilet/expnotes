package com.expnotes.model;

/**
 * Created by jmimora on 26/11/16.
 */

public class NotaText extends Nota {
    public NotaText(String email, String expedient) {
        super(email, expedient);
    }

    public int tipusNota() {
        return Nota.NOTA_TEXT;
    }

    public String getNomFitxer() {
        return null;
    }
}
