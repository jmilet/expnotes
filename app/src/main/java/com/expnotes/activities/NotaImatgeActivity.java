package com.expnotes.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.expnotes.R;
import com.expnotes.fragments.NotaImatgeFragment;


public class NotaImatgeActivity extends AppCompatActivity {

    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.identificador_expedient";
    public static final String ARG_IDENTIFICADOR_NOTA = "com.expnotes.identificador_nota";

    public static Intent newIntent(Context packageContext, String email, String idExp, String idNota) {
        Intent intent = new Intent(packageContext, NotaImatgeActivity.class);

        intent.putExtra(ARG_EMAIL, email);
        intent.putExtra(ARG_IDENTIFICADOR_EXPEDIENT, idExp);
        intent.putExtra(ARG_IDENTIFICADOR_NOTA, idNota);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_expedients);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String email = getIntent().getStringExtra(ARG_EMAIL);
        String idExp = getIntent().getStringExtra(ARG_IDENTIFICADOR_EXPEDIENT);
        String idNota = getIntent().getStringExtra(ARG_IDENTIFICADOR_NOTA);

        hideFloatingButton();
        setTitleBar(idExp, idNota);
        createFragment(email, idExp, idNota);
    }

    private void hideFloatingButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();
    }

    private void setTitleBar(String idExp, String idNota) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(String.format(getString(R.string.text_titol_creacio_nota), idExp, idNota));
    }

    private void createFragment(String email, String idExp, String idNota) {
        // Fragment.
        FragmentManager fm = getSupportFragmentManager();
        Fragment notaFragment = fm.findFragmentById(R.id.content_expedients);
        if (notaFragment == null) {
            notaFragment = NotaImatgeFragment.newInstance(email, idExp, idNota);
            fm.beginTransaction().add(R.id.content_expedients, notaFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_expedient, menu);
        return true;
    }
}
