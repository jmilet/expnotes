package com.expnotes.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.expnotes.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText emailEditText = (EditText) findViewById(R.id.edit_text_email);
        emailEditText.setText("default@default.com");

        Button buttonLogin = (Button) findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String email = emailEditText.getText().toString();

                Intent intent = ExpedientListActivity.newIntent(MainActivity.this, email);

                startActivity(intent);
                MainActivity.this.finish();
            }
        });
    }
}
