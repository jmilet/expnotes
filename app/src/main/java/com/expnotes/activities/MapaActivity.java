package com.expnotes.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.expnotes.R;
import com.expnotes.fragments.MapaFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by jmimora on 27/11/16.
 */

public class MapaActivity extends AppCompatActivity {

    public static final String ARG_IDENTIFICADOR_TITLE = "com.expnotes.title";
    public static final String ARG_LATITUD = "com.expnotes.latitud";
    public static final String ARG_LONGITUD = "com.expnotes.longitud";

    public static Intent newIntent(Context packageContext, String title, LatLng posicioGPS) {
        Intent intent = new Intent(packageContext, MapaActivity.class);

        intent.putExtra(ARG_IDENTIFICADOR_TITLE, title);
        intent.putExtra(ARG_LATITUD, posicioGPS.latitude);
        intent.putExtra(ARG_LONGITUD, posicioGPS.longitude);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_expedients);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String title = getIntent().getStringExtra(ARG_IDENTIFICADOR_TITLE);
        double longitud = getIntent().getDoubleExtra(ARG_LONGITUD, -1);
        double latitud = getIntent().getDoubleExtra(ARG_LATITUD, -1);

        hideFloatingButton();
        setTitleBar(title);
        createFragment(latitud, longitud);
    }

    private void hideFloatingButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();
    }

    private void setTitleBar(String title) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    private void createFragment(double latitud, double longitud) {
        // Fragment.
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.content_expedients);
        if (fragment == null) {
            fragment = MapaFragment.newInstance(latitud, longitud);
            fm.beginTransaction().add(R.id.content_expedients, fragment).commit();
        }
    }
}

