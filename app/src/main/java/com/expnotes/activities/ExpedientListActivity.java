package com.expnotes.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.NavigationView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expnotes.fragments.ExpedientListFragment;
import com.expnotes.R;
import com.expnotes.model.Expedient;
import com.expnotes.persistencia.GestorPersistencia;
import com.google.android.gms.maps.model.LatLng;

public class ExpedientListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_EMAIL = "com.expnotes.email";

    private static final String THIS_SCOPE = "ExpedientListActivity";
    private static final String TITOL = "titol";

    private ExpedientListFragment mFragment;
    private String mEmail;

    public static Intent newIntent(Context packageContext, String email) {
        Intent intent = new Intent(packageContext, ExpedientListActivity.class);
        intent.putExtra(ARG_EMAIL, email);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_expedients);

        mEmail = getIntent().getStringExtra(ARG_EMAIL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(GestorPersistencia.getStringPreference(this, THIS_SCOPE, TITOL, titol("Tots")));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        createFragment();
    }

    private void createFragment() {
        FragmentManager fm = getSupportFragmentManager();
        mFragment = (ExpedientListFragment) fm.findFragmentById(R.id.content_expedients);
        if (mFragment == null) {
            mFragment = ExpedientListFragment.newInstance(mEmail);
            fm.beginTransaction().add(R.id.content_expedients, mFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_list_expedients, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mFragment.setSearchText(query);
                mFragment.updateUI();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mFragment.setSearchText(newText);
                mFragment.updateUI();
                return true;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mFragment.setSearchText(null);
                mFragment.updateUI();
                return false;
            }
        });

        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        GestorPersistencia.setStringPreference(this, THIS_SCOPE, TITOL, getSupportActionBar().getTitle().toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setTitle(GestorPersistencia.getStringPreference(this, THIS_SCOPE, TITOL, titol("Tots")));
    }

    private String titol(String subTitol) {
        return getString(R.string.text_titol_llista_expedients) + " " + subTitol;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_sortir:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                this.finish();
                break;

            case R.id.nav_sincronitzar:
                mFragment.fetchExpedients();
                break;

            case R.id.nav_ordenar_cronologia:
                mFragment.setOrdreCronologic();
                mFragment.updateUI();
                break;

            case R.id.nav_ordenar_prioritat:
                mFragment.setOrdrePrioritat();
                mFragment.updateUI();
                break;

            case R.id.nav_ordenar_proximitat_prioritat:
                mFragment.setOrdreProximitatPrioritat();
                mFragment.updateUI();
                break;

            case R.id.nav_ordenar_prioritat_proximitat:
                mFragment.setOrdrePrioritatProximitat();
                mFragment.updateUI();
                break;

            case R.id.nav_filtre_oberts:
                mFragment.setFiltreGeneralOberts();
                mFragment.updateUI();
                getSupportActionBar().setTitle(titol(getString(R.string.obert_label)));
                break;

            case R.id.nav_filtre_espera:
                mFragment.setFiltreGeneralEspera();
                mFragment.updateUI();
                getSupportActionBar().setTitle(titol(getString(R.string.en_espera_label)));
                break;

            case R.id.nav_filtre_tancats:
                mFragment.setFiltreGeneralTancats();
                mFragment.updateUI();
                getSupportActionBar().setTitle(titol(getString(R.string.tancats_label)));
                break;

            case R.id.nav_filtre_tots:
                mFragment.setFiltreGeneralTots();
                mFragment.updateUI();
                getSupportActionBar().setTitle(titol(getString(R.string.tots_label)));
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
