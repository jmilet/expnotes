package com.expnotes.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.expnotes.R;
import com.expnotes.activities.DetallExpedientActivity;
import com.expnotes.activities.MapaActivity;
import com.expnotes.activities.NotaListActivity;
import com.expnotes.model.Expedient;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.DateTimeHelper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

public class ExpedientFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.idexp";

    private EditText mTitolEditText;
    private EditText mDescripcioEditText;
    private BottomNavigationView mBottomNavigationView;
    private GoogleApiClient mClient;
    private Location mLastLocation;

    private Expedient mExpedient;
    private String[] mEstatsKeysArray;
    private String[] mPrioritatsKeysArray;

    public ExpedientFragment() {
    }

    public static ExpedientFragment newInstance(String email, String idExp) {
        Bundle args = new Bundle();
        args.putString(ExpedientFragment.ARG_EMAIL, email);
        args.putString(ExpedientFragment.ARG_IDENTIFICADOR_EXPEDIENT, idExp);

        ExpedientFragment fragment = new ExpedientFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        String email = getArguments().getString(ARG_EMAIL);
        String id = getArguments().getString(ARG_IDENTIFICADOR_EXPEDIENT);
        mExpedient = GestorPersistencia.getInstance(getActivity()).getExpedientById(email, id);

        mEstatsKeysArray = getResources().getStringArray(R.array.estats_keys_array);
        mPrioritatsKeysArray = getResources().getStringArray(R.array.prioritats_keys_array);

        if (mExpedient.getLocation() == null ) {
            mClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mClient);
                            mExpedient.setLocation(mLastLocation);

                            LocationRequest request = LocationRequest.create();
                            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            request.setNumUpdates(1);
                            request.setInterval(0);

                            LocationServices.FusedLocationApi.requestLocationUpdates(mClient, request, new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    mLastLocation = location;
                                    mExpedient.setLocation(mLastLocation);
                                }
                            });
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mExpedient.getLocation() == null) mClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mClient != null) mClient.disconnect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expedient, container, false);

        // Estats.
        Spinner spinnerEstats = (Spinner) view.findViewById(R.id.spiner_stats);
        ArrayAdapter<CharSequence> adapterEstats = ArrayAdapter.createFromResource(getActivity(), R.array.estats_values_array, android.R.layout.simple_spinner_item);
        adapterEstats.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstats.setAdapter(adapterEstats);
        spinnerEstats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String estat = mEstatsKeysArray[pos];
                if (!mExpedient.getEstat().equals(estat)) mExpedient.setEstat(estat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spinnerEstats.setSelection(indexPerAKey(mEstatsKeysArray, mExpedient.getEstat()));

        // Prioritats
        Spinner spinnerPrioritats = (Spinner) view.findViewById(R.id.spiner_prioritats);
        ArrayAdapter<CharSequence> adapterPrioritats = ArrayAdapter.createFromResource(getActivity(), R.array.prioritats_values_array, android.R.layout.simple_spinner_item);
        adapterPrioritats.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPrioritats.setAdapter(adapterPrioritats);
        spinnerPrioritats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String prioritat = mPrioritatsKeysArray[pos];
                mExpedient.setPrioritat(Integer.valueOf(prioritat));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spinnerPrioritats.setSelection(indexPerAKey(mPrioritatsKeysArray, String.valueOf(mExpedient.getPrioritat())));

        mTitolEditText = (EditText) view.findViewById(R.id.edit_text_titol);
        mDescripcioEditText = (EditText) view.findViewById(R.id.edit_text_descripcio);

        mBottomNavigationView = (BottomNavigationView) view.findViewById(R.id.navigation);
        mBottomNavigationView.inflateMenu(R.menu.menu_bottom_expedient_navigation_items);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        bindModelToView();
        setHasOptionsMenu(true);

        return view;
    }

    private void bindModelToView() {
        mTitolEditText.setText(mExpedient.getTitol());
        mDescripcioEditText.setText(mExpedient.getDescripcio());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                demanarConfirmacioUsuariPerEsborrarExpedient();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.action_notes:
                intent = NotaListActivity.newIntent(getActivity(), mExpedient.getEmail(), mExpedient.getId());
                startActivity(intent);
                return true;

            case R.id.action_detall_expedient:
                intent = DetallExpedientActivity.newIntent(getActivity(), mExpedient.getEmail(), mExpedient.getId());
                startActivity(intent);
                return true;

            case R.id.action_mapa:
                if (mExpedient.getLocation() == null) {
                    Toast.makeText(getActivity(), getString(R.string.text_coordenades_gps_no_disponibles), Toast.LENGTH_LONG).show();
                    return false;
                }
                else {
                    intent = MapaActivity.newIntent(getActivity(), mExpedient.getTitol(), new LatLng(mExpedient.getLocation().getLatitude(), mExpedient.getLocation().getLongitude()));
                    startActivity(intent);
                    return true;
                }
        }

        return false;
    }

    private void demanarConfirmacioUsuariPerEsborrarExpedient() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.text_confirmacio))
                .setMessage(getString(R.string.text_pregunta_esborrar_expedient))
                .setPositiveButton(getString(R.string.text_si_boto_dialog), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GestorPersistencia.getInstance(getActivity()).eliminarExpedientById(mExpedient.getEmail(), mExpedient.getId());
                        getActivity().finish();
                    }

                })
                .setNegativeButton(R.string.text_no_boto_dialog, null)
                .show();
    }

    @Override
    public void onPause() {
        super.onPause();
        mExpedient.setTitol(mTitolEditText.getText().toString());
        mExpedient.setDescripcio(mDescripcioEditText.getText().toString());
        GestorPersistencia.getInstance(getActivity()).updateExpedient(mExpedient);
    }

    private int indexPerAKey(String[] keys, String key) {
        for (int i = 0; i < keys.length; i++) {
            if (keys[i].equals(key)) {
                return i;
            }
        }

        return 0;
    }
}
