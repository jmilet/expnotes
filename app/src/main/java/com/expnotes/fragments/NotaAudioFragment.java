package com.expnotes.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.expnotes.R;
import com.expnotes.activities.DetallNotaActivity;
import com.expnotes.activities.MapaActivity;
import com.expnotes.model.NotaAudio;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.DefinedColors;
import com.expnotes.utils.FileUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.IOException;


/**
 * Created by jmimora on 26/11/16.
 */


public class NotaAudioFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.idexp";
    public static final String ARG_IDENTIFICADOR_NOTA = "com.expnotes.idnota";

    private static final int REQUEST_IMATGE = 0;

    private EditText mTitolEditText;
    private ImageView mImatgeView;
    private BottomNavigationView mBottomNavigationView;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private ImageButton mButtonPlay;
    private ImageButton mButtonRec;
    private GoogleApiClient mClient;
    private Location mLastLocation;

    private NotaAudio mNota;
    private File mFitxerSo;
    private boolean gravant;
    private boolean reproduint;

    public NotaAudioFragment() {
    }

    public static NotaAudioFragment newInstance(String email, String idExp, String idNota) {
        Bundle args = new Bundle();
        args.putString(ARG_EMAIL, email);
        args.putString(ARG_IDENTIFICADOR_EXPEDIENT, idExp);
        args.putString(ARG_IDENTIFICADOR_NOTA, idNota);

        NotaAudioFragment fragment = new NotaAudioFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String email = getArguments().getString(ARG_EMAIL);
        String mIdExp = getArguments().getString(ARG_IDENTIFICADOR_EXPEDIENT);
        String idNota = getArguments().getString(ARG_IDENTIFICADOR_NOTA);

        mNota = (NotaAudio) GestorPersistencia.getInstance(getActivity()).getNotaById(email, mIdExp, idNota);
        mFitxerSo = FileUtils.getFileInStorage(getActivity(), mNota.getNomFitxer());

        if (mNota.getLocation() == null) {
            mClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mClient);
                            mNota.setLocation(mLastLocation);

                            LocationRequest request = LocationRequest.create();
                            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            request.setNumUpdates(1);
                            request.setInterval(0);

                            LocationServices.FusedLocationApi.requestLocationUpdates(mClient, request, new com.google.android.gms.location.LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    mLastLocation = location;
                                    mNota.setLocation(mLastLocation);
                                }
                            });
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mNota.getLocation() == null) mClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mClient != null) mClient.disconnect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nota_audio, container, false);

        gravant = false;
        reproduint = false;

        mTitolEditText = (EditText) view.findViewById(R.id.edit_text_titol);
        mImatgeView = (ImageView) view.findViewById(R.id.imatge_view_foto);
        mButtonPlay = (ImageButton) view.findViewById(R.id.image_button_play);
        mButtonRec = (ImageButton) view.findViewById(R.id.image_button_rec);

        mBottomNavigationView = (BottomNavigationView) view.findViewById(R.id.navigation);
        mBottomNavigationView.inflateMenu(R.menu.menu_bottom_nota_audio_navigation_items);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);


        mButtonRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!gravant) {
                    iniciarGravacio();
                } else {
                    aturarGravacio();
                }
            }
        });

        mButtonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!reproduint) {
                    iniciarReproduccio();
                } else {
                    aturarReproduccio();
                }
            }
        });

        bindModelToView();
        setHasOptionsMenu(true);

        return view;
    }

    private void iniciarReproduccio() {
        reproduint = true;
        recInactiu();
        playReproduint();

        mPlayer = new MediaPlayer();

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                aturarReproduccio();
            }
        });

        try {

            mPlayer.setDataSource(mFitxerSo.getPath());
            mPlayer.prepare();
            mPlayer.start();

        } catch (IOException e) {
            Log.e("ERROR", "prepare() failed");
        }
    }

    private void aturarReproduccio() {
        reproduint = false;
        playEsperant();
        recEsperant();
        mPlayer.release();
        mPlayer = null;
    }

    private void iniciarGravacio() {
        gravant = true;
        recGravant();
        playInactiu();

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFitxerSo.getPath());
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("ERROR", "prepare() failed");
        }

        mRecorder.start();
    }

    private void aturarGravacio() {
        gravant = false;
        recEsperant();
        playEsperant();

        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

    private void playInactiu() {
        mButtonPlay.setEnabled(true);
        mButtonPlay.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
        mButtonPlay.setColorFilter(DefinedColors.gray());
    }

    private void playEsperant() {
        mButtonPlay.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
        mButtonPlay.setEnabled(true);
        mButtonPlay.setColorFilter(DefinedColors.blue());
    }

    private void playReproduint() {
        mButtonPlay.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
        mButtonPlay.setEnabled(true);
        mButtonPlay.setColorFilter(DefinedColors.red());
    }

    private void recInactiu() {
        mButtonRec.setEnabled(false);
        mButtonRec.setColorFilter(DefinedColors.gray());
    }

    private void recEsperant() {
        mButtonRec.setEnabled(true);
        mButtonRec.setColorFilter(DefinedColors.cyan());
    }

    private void recGravant() {
        mButtonRec.setEnabled(true);
        mButtonRec.setColorFilter(DefinedColors.red());
    }

    private void bindModelToView() {
        mTitolEditText.setText(mNota.getTitol());
        updateSoundView();
    }

    private void updateSoundView() {
        if (mFitxerSo == null || !mFitxerSo.exists()) {
            recEsperant();
            playInactiu();
        } else {
            recEsperant();
            playEsperant();
        }
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFitxerSo.getPath());
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("ERROR", "prepare() failed");
        }

        mRecorder.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                demanarConfirmacioUsuariPerEsborrarNota();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.action_detall_nota:
                intent = DetallNotaActivity.newIntent(getActivity(), mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                startActivity(intent);
                return true;

            case R.id.action_mapa:
                if (mNota.getLocation() == null) {
                    Toast.makeText(getActivity(), getString(R.string.text_coordenades_gps_no_disponibles), Toast.LENGTH_LONG).show();
                    return false;
                }
                else {
                    intent = MapaActivity.newIntent(getActivity(), mNota.getTitol(), new LatLng(mNota.getLocation().getLatitude(), mNota.getLocation().getLongitude()));
                    startActivity(intent);
                    return true;
                }
        }

        return false;
    }

    private void demanarConfirmacioUsuariPerEsborrarNota() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.text_confirmacio))
                .setMessage(getString(R.string.text_pregunta_esborrar_nota))
                .setPositiveButton(getString(R.string.text_si_boto_dialog), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GestorPersistencia.getInstance(getActivity()).deleteNotaById(mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                        mFitxerSo.delete();
                        getActivity().finish();
                    }

                })
                .setNegativeButton(R.string.text_no_boto_dialog, null)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_IMATGE:
                updateSoundView();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mNota.setTitol(mTitolEditText.getText().toString());
        GestorPersistencia.getInstance(getActivity()).updateNota(mNota);
    }
}

