package com.expnotes.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expnotes.R;
import com.expnotes.activities.DetallExpedientActivity;
import com.expnotes.model.Expedient;
import com.expnotes.model.Nota;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.DateTimeHelper;

public class DetallExpedientFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.idexp";

    private TextView mMomentCreacioTextView;
    private TextView mMomentTancamentTextView;
    private TextView mLatitudTextView;
    private TextView mLongitudTextView;
    private TextView mEstatTextView;

    private Expedient mExpedient;

    public DetallExpedientFragment() {
    }

    public static DetallExpedientFragment newInstance(String email, String idExp) {
        Bundle args = new Bundle();

        args.putString(DetallExpedientFragment.ARG_EMAIL, email);
        args.putString(DetallExpedientFragment.ARG_IDENTIFICADOR_EXPEDIENT, idExp);

        DetallExpedientFragment fragment = new DetallExpedientFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String email = getArguments().getString(ARG_EMAIL);
        String idExp = getArguments().getString(ARG_IDENTIFICADOR_EXPEDIENT);

        mExpedient = GestorPersistencia.getInstance(getActivity()).getExpedientById(email, idExp);
    }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detall_expedient, container, false);

        mMomentCreacioTextView = (TextView) view.findViewById(R.id.text_view_moment_creacio);
        mMomentTancamentTextView = (TextView) view.findViewById(R.id.text_view_moment_tancament);
        mLatitudTextView = (TextView) view.findViewById(R.id.text_view_latitud);
        mLongitudTextView = (TextView) view.findViewById(R.id.text_view_longitud);
        mEstatTextView = (TextView) view.findViewById(R.id.text_view_estat);

        bindModelToView();
        setHasOptionsMenu(true);

        return view;
    }

    private void bindModelToView() {
        mMomentCreacioTextView.setText(DateTimeHelper.unixEpochToString(mExpedient.getMomentCreacio()));

        if (mExpedient.getEstat() == Expedient.ESTAT_TANCAT) {
            mMomentTancamentTextView.setText(DateTimeHelper.unixEpochToString(mExpedient.getMomentTancament()));
        }

        if (mExpedient.getLocation() != null) {
            mLatitudTextView.setText(String.valueOf(mExpedient.getLocation().getLatitude()));
            mLongitudTextView.setText(String.valueOf(mExpedient.getLocation().getLongitude()));
        }

        mEstatTextView.setText(mExpedient.getLiteralEstat());
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
