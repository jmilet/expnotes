package com.expnotes.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.expnotes.R;

/**
 * Created by jmimora on 20/11/16.
 */

public class EscullirTipusNotaDialogFragment extends DialogFragment {

    public static final String EXTRA_TIPUS_NOTA = "com.expnotes.tipus_nota";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.text_menu_tipus_notes))
                .setItems(R.array.string_array_menu_notes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sendResult(Activity.RESULT_OK, which);
                    }
                });
        return builder.create();
    }

    private void sendResult(int resultCode, int which) {
        if (getTargetFragment() == null) return;

        Intent intent = new Intent();
        intent.putExtra(EXTRA_TIPUS_NOTA, which);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }
}
