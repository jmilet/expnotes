package com.expnotes.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expnotes.R;
import com.expnotes.model.Expedient;
import com.expnotes.model.Nota;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.DateTimeHelper;

public class DetallNotaFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.idexp";
    public static final String ARG_IDENTIFICADOR_NOTA = "com.expnotes.idnota";

    private TextView mMomentCreacioTextView;
    private TextView mLatitudTextView;
    private TextView mLongitudTextView;

    private Nota mNota;

    public DetallNotaFragment() {
    }

    public static DetallNotaFragment newInstance(String email, String idExp, String idNota) {
        Bundle args = new Bundle();

        args.putString(DetallNotaFragment.ARG_EMAIL, email);
        args.putString(DetallNotaFragment.ARG_IDENTIFICADOR_EXPEDIENT, idExp);
        args.putString(DetallNotaFragment.ARG_IDENTIFICADOR_NOTA, idNota);

        DetallNotaFragment fragment = new DetallNotaFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String email = getArguments().getString(ARG_EMAIL);
        String idExp = getArguments().getString(ARG_IDENTIFICADOR_EXPEDIENT);
        String idNota = getArguments().getString(ARG_IDENTIFICADOR_NOTA);
        mNota = GestorPersistencia.getInstance(getActivity()).getNotaById(email, idExp, idNota);
    }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detall_nota, container, false);

        mMomentCreacioTextView = (TextView) view.findViewById(R.id.text_view_moment_creacio);
        mLatitudTextView = (TextView) view.findViewById(R.id.text_view_latitud);
        mLongitudTextView = (TextView) view.findViewById(R.id.text_view_longitud);

        bindModelToView();
        setHasOptionsMenu(true);

        return view;
    }

    private void bindModelToView() {
        mMomentCreacioTextView.setText(DateTimeHelper.unixEpochToString(mNota.getMomentCreacio()));

        if (mNota.getLocation() != null) {
            mLatitudTextView.setText(String.valueOf(mNota.getLocation().getLatitude()));
            mLongitudTextView.setText(String.valueOf(mNota.getLocation().getLongitude()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                demanarConfirmacioUsuariPerEsborrarNota();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    private void demanarConfirmacioUsuariPerEsborrarNota() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.text_confirmacio))
                .setMessage(getString(R.string.text_pregunta_esborrar_expedient))
                .setPositiveButton(getString(R.string.text_si_boto_dialog), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GestorPersistencia.getInstance(getActivity()).deleteNotaById(mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                        getActivity().finish();
                    }

                })
                .setNegativeButton(R.string.text_no_boto_dialog, null)
                .show();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
