package com.expnotes.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.expnotes.R;
import com.expnotes.activities.DetallNotaActivity;
import com.expnotes.activities.MapaActivity;
import com.expnotes.model.NotaImatge;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.FileUtils;
import com.expnotes.utils.PictureUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;

/**
 * Created by jmimora on 26/11/16.
 */


public class NotaImatgeFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.idexp";
    public static final String ARG_IDENTIFICADOR_NOTA = "com.expnotes.idnota";

    private static final int REQUEST_IMATGE = 0;

    private EditText mTitolEditText;
    private ImageView mImatgeView;
    private BottomNavigationView mBottomNavigationView;
    private GoogleApiClient mClient;
    private Location mLastLocation;

    //private String mIdExp;
    private NotaImatge mNota;
    private File mImatge;

    public NotaImatgeFragment() {
    }

    public static NotaImatgeFragment newInstance(String email, String idExp, String idNota) {
        Bundle args = new Bundle();
        args.putString(ARG_EMAIL, email);
        args.putString(ARG_IDENTIFICADOR_EXPEDIENT, idExp);
        args.putString(ARG_IDENTIFICADOR_NOTA, idNota);

        NotaImatgeFragment fragment = new NotaImatgeFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String email = getArguments().getString(ARG_EMAIL);
        String idExp = getArguments().getString(ARG_IDENTIFICADOR_EXPEDIENT);
        String idNota = getArguments().getString(ARG_IDENTIFICADOR_NOTA);

        mNota = (NotaImatge) GestorPersistencia.getInstance(getActivity()).getNotaById(email, idExp, idNota);
        mImatge = FileUtils.getFileInStorage(getActivity(), mNota.getNomFitxer());

        if (mNota.getLocation() == null) {
            mClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mClient);
                            mNota.setLocation(mLastLocation);

                            LocationRequest request = LocationRequest.create();
                            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            request.setNumUpdates(1);
                            request.setInterval(0);

                            LocationServices.FusedLocationApi.requestLocationUpdates(mClient, request, new com.google.android.gms.location.LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    mLastLocation = location;
                                    mNota.setLocation(mLastLocation);
                                }
                            });
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mNota.getLocation() == null) mClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mClient != null) mClient.disconnect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nota_imatge, container, false);

        mTitolEditText = (EditText) view.findViewById(R.id.edit_text_titol);
        mImatgeView = (ImageView) view.findViewById(R.id.imatge_view_foto);

        mBottomNavigationView = (BottomNavigationView) view.findViewById(R.id.navigation);
        mBottomNavigationView.inflateMenu(R.menu.menu_bottom_nota_imatge_navigation_items);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        mImatge = FileUtils.getFileInStorage(getActivity(), mNota.getNomFitxer());

        bindModelToView();
        setHasOptionsMenu(true);

        return view;
    }

    private void bindModelToView() {
        mTitolEditText.setText(mNota.getTitol());
        updatePhotoView();
    }

    private void updatePhotoView() {
        if (mImatge == null || !mImatge.exists()) {
            //mImatgeView.setImageDrawable(null);
        }
        else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(mImatge.getPath(), getActivity());
            mImatgeView.setImageBitmap(bitmap);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                demanarConfirmacioUsuariPerEsborrarNota();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.action_imatge:
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                // TODO: Check camera

                //boolean hihaCamera = mImatge != null && intent.resolveActivity(packageManager) != null;

                Uri uri = Uri.fromFile(mImatge);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, REQUEST_IMATGE);
                return true;

            case R.id.action_detall_nota:
                intent = DetallNotaActivity.newIntent(getActivity(), mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                startActivity(intent);
                return true;

            case R.id.action_mapa:
                if (mNota.getLocation() == null) {
                    Toast.makeText(getActivity(), getString(R.string.text_coordenades_gps_no_disponibles), Toast.LENGTH_LONG).show();
                    return false;
                }
                else {
                    intent = MapaActivity.newIntent(getActivity(), mNota.getTitol(), new LatLng(mNota.getLocation().getLatitude(), mNota.getLocation().getLongitude()));
                    startActivity(intent);
                    return true;
                }
        }

        return false;
    }

    private void demanarConfirmacioUsuariPerEsborrarNota() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.text_confirmacio))
                .setMessage(getString(R.string.text_pregunta_esborrar_nota))
                .setPositiveButton(getString(R.string.text_si_boto_dialog), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GestorPersistencia.getInstance(getActivity()).deleteNotaById(mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                        mImatge.delete();
                        getActivity().finish();
                    }

                })
                .setNegativeButton(R.string.text_no_boto_dialog, null)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_IMATGE:
                updatePhotoView();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mNota.setTitol(mTitolEditText.getText().toString());
        GestorPersistencia.getInstance(getActivity()).updateNota(mNota);
    }
}

