package com.expnotes.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.expnotes.R;
import com.expnotes.activities.NotaListActivity;
import com.expnotes.model.Expedient;
import com.expnotes.persistencia.GestorPersistencia;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaFragment extends SupportMapFragment {

    public static final String ARG_LATITUD = "com.expnotes.latitud";
    public static final String ARG_LONGITUD = "com.expnotes.longitud";

    private GoogleMap mMap;
    private BottomNavigationView mBottomNavigationView;
    private LatLng mPosicioGPS;

    public MapaFragment() {
    }

    public static MapaFragment newInstance(double latitud, double longitud) {
        Bundle args = new Bundle();
        args.putDouble(MapaFragment.ARG_LATITUD, latitud);
        args.putDouble(MapaFragment.ARG_LONGITUD, longitud);

        MapaFragment fragment = new MapaFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        mPosicioGPS = new LatLng(getArguments().getDouble(ARG_LATITUD), getArguments().getDouble(ARG_LONGITUD));

        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.setMyLocationEnabled(true);
                updateUI();
            }
        });

    }

    private void updateUI() {
        if (mMap == null) {
            return;
        }

        MarkerOptions itemMarker = new MarkerOptions().position(mPosicioGPS);
        mMap.clear();
        mMap.addMarker(itemMarker);

        //LatLngBounds bound = new LatLngBounds.Builder().include(mPosicioGPS).build();
        //CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bound, 0);
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(mPosicioGPS.latitude, mPosicioGPS.longitude) , 11.0f) );
        //mMap.animateCamera(update);
    }
}
