package com.expnotes.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.expnotes.DividerItemDecoration;
import com.expnotes.R;
import com.expnotes.activities.NotaAudioActivity;
import com.expnotes.activities.NotaImatgeActivity;
import com.expnotes.activities.NotaTextActivity;
import com.expnotes.model.Expedient;
import com.expnotes.model.Nota;
import com.expnotes.model.NotaAudio;
import com.expnotes.model.NotaImatge;
import com.expnotes.model.NotaText;
import com.expnotes.persistencia.DbSchema;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.DefinedColors;

import java.util.List;



public class NotaListFragment extends Fragment {

    public static final String THIS_SCOPE = "NotaListFragment";
    public static final String ARG_EMAIL = "com.expnotes.email";
    public static final String ARG_IDENTIFICADOR_EXPEDIENT = "com.expnotes.idexp";

    private static final int REQUEST_TIPUS_NOTA = 0;

    private RecyclerView mExpedientsRecyclerView;
    private NotaAdapter mAdapter;
    private String mEmail;
    private String mIdExp;

    public NotaListFragment() {
    }

    public static NotaListFragment newInstance(String email, String identificadorExpedient) {
        Bundle args = new Bundle();

        args.putString(ARG_EMAIL, email);
        args.putString(ARG_IDENTIFICADOR_EXPEDIENT, identificadorExpedient);

        NotaListFragment fragment = new NotaListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    private class NotaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTitolTextView;
        private ImageView mImageViewIcon;
        private Nota mNota;

        public NotaHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitolTextView = (TextView) itemView.findViewById(R.id.text_view_titol);
            mImageViewIcon = (ImageView) itemView.findViewById(R.id.ic_item_expedient);
        }

        public void bindNota(Nota nota) {
            mNota = nota;
            mTitolTextView.setText(nota.getTitol());

            switch (mNota.tipusNota()) {
                case Nota.NOTA_TEXT:
                    mImageViewIcon.setImageResource(R.drawable.ic_edit_black_24dp);
                    mImageViewIcon.setColorFilter(DefinedColors.red());
                    break;

                case Nota.NOTA_IMATGE:
                    mImageViewIcon.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                    mImageViewIcon.setColorFilter(DefinedColors.blue());
                    break;

                case Nota.NOTA_AUDIO:
                    mImageViewIcon.setImageResource(R.drawable.ic_mic_black_24dp);
                    mImageViewIcon.setColorFilter(DefinedColors.cyan());
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent intent = null;

            switch (mNota.tipusNota()) {
                case Nota.NOTA_TEXT:
                    intent = NotaTextActivity.newIntent(getActivity(), mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                    break;

                case Nota.NOTA_IMATGE:
                    intent = NotaImatgeActivity.newIntent(getActivity(), mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                    break;

                case Nota.NOTA_AUDIO:
                    intent = NotaAudioActivity.newIntent(getActivity(), mNota.getEmail(), mNota.getExpedient(), mNota.getId());
                    break;
            }
            startActivity(intent);
        }
    }

    private class NotaAdapter extends RecyclerView.Adapter<NotaHolder> {
        private List<Nota> mNotes;

        public NotaAdapter(List<Nota> notes) {
            mNotes = notes;
        }

        @Override
        public NotaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_list_nota, parent, false);
            return new NotaHolder(view);
        }

        @Override
        public void onBindViewHolder(NotaHolder holder, int position) {
            Nota nota = mNotes.get(position);
            holder.bindNota(nota);
        }

        @Override
        public int getItemCount() {
            return mNotes.size();
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        mEmail = getArguments().getString(ARG_EMAIL);
        mIdExp = getArguments().getString(ARG_IDENTIFICADOR_EXPEDIENT);

        if (mEmail == null) {
            mEmail = GestorPersistencia.getStringPreference(getActivity(), THIS_SCOPE, ARG_EMAIL, "");
            mIdExp = GestorPersistencia.getStringPreference(getActivity(), THIS_SCOPE, ARG_IDENTIFICADOR_EXPEDIENT, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_notes, container, false);

        mExpedientsRecyclerView = (RecyclerView) view.findViewById(R.id.expedients_recycler_view);
        mExpedientsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        mExpedientsRecyclerView.addItemDecoration(dividerItemDecoration);

        setFloatingActionButton();
        updateUI(getNotes());

        return view;
    }

    private void setFloatingActionButton() {
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment fragment = new EscullirTipusNotaDialogFragment();
                fragment.setTargetFragment(NotaListFragment.this, REQUEST_TIPUS_NOTA);
                fragment.show(getActivity().getSupportFragmentManager(), "tipus_nota");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == REQUEST_TIPUS_NOTA) {
            Nota nota;
            Intent intent;

            int which = data.getIntExtra(EscullirTipusNotaDialogFragment.EXTRA_TIPUS_NOTA, -1);
            switch (which) {
                case 0:
                    nota = new NotaText(mEmail, mIdExp);
                    nota.setTitol("Títol");

                    GestorPersistencia.getInstance(getActivity()).addNota(nota);
                    intent = NotaTextActivity.newIntent(getActivity(), nota.getEmail(), nota.getExpedient(), nota.getId());
                    startActivity(intent);
                    break;

                case 1:
                    nota = new NotaImatge(mEmail, mIdExp);
                    nota.setTitol("Títol");

                    GestorPersistencia.getInstance(getActivity()).addNota(nota);
                    intent = NotaImatgeActivity.newIntent(getActivity(), nota.getEmail(), nota.getExpedient(), nota.getId());
                    startActivity(intent);
                    break;

                case 2:
                    nota = new NotaAudio(mEmail, mIdExp);
                    nota.setTitol("Títol");

                    GestorPersistencia.getInstance(getActivity()).addNota(nota);
                    intent = NotaAudioActivity.newIntent(getActivity(), nota.getEmail(), nota.getExpedient(), nota.getId());
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI(getNotes());
    }

    @Override
    public void onPause() {
        super.onPause();
        GestorPersistencia.setStringPreference(getActivity(), THIS_SCOPE, ARG_EMAIL, mEmail);
        GestorPersistencia.setStringPreference(getActivity(), THIS_SCOPE, ARG_IDENTIFICADOR_EXPEDIENT, mIdExp);
    }

    private void updateUI(List<Nota> notes) {
        mAdapter = new NotaAdapter(notes);
        mExpedientsRecyclerView.setAdapter(mAdapter);
    }

    private List<Nota> getNotes() {
        String whereClause = String.format("%s = ? AND %s = ?", DbSchema.NotaTable.Cols.EMAIL, DbSchema.NotaTable.Cols.EXPEDIENT);
        String[] whereArgs = new String[] { mEmail, mIdExp };
        String orderBy = DbSchema.NotaTable.Cols.ID;

        return GestorPersistencia.getInstance(getActivity()).getNotes(whereClause, whereArgs, orderBy);
    }
}
