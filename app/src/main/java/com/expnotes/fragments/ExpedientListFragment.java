package com.expnotes.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.expnotes.DividerItemDecoration;
import com.expnotes.R;
import com.expnotes.activities.ExpedientActivity;
import com.expnotes.model.Expedient;
import com.expnotes.model.Nota;
import com.expnotes.model.NotaAudio;
import com.expnotes.model.NotaImatge;
import com.expnotes.model.NotaText;
import com.expnotes.persistencia.DbSchema;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.persistencia.HttpFetcher;
import com.expnotes.utils.DateTimeHelper;
import com.expnotes.utils.DefinedColors;
import com.expnotes.utils.GestorOrdenacionsEspecials;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;



public class ExpedientListFragment extends Fragment {

    public static final String THIS_SCOPE = "ExpedientListFragment";
    public static final String ORDRE_EXPEDIENTS = "ordreExpedients";
    public static final String FILTRE_EXPEDIENTS = "filtreExpedients";
    public static final String ORDER_BY_PROXIMITAT_PRIORITAT = "proximitat_prioritat";
    public static final String ORDER_BY_PRIORITAT_PROXIMITAT = "prioritat_proximitat";
    public static final String ARG_EMAIL = "com.expnotes.email";

    private RecyclerView mExpedientsRecyclerView;
    private ImageView mIconImageView;
    private ExpedientAdapter mAdapter;

    private GoogleApiClient mClient;
    private Location mLastLocation;

    private String mEmail;
    private String mSearchText;
    private String mOrderBy;
    private String mWhereFilter;

    public ExpedientListFragment() {
    }

    public static ExpedientListFragment newInstance(String email) {
        Bundle args = new Bundle();
        args.putString(ARG_EMAIL, email);

        ExpedientListFragment fragment = new ExpedientListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    private class ExpedientHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mIdentificadorTextView;
        private TextView mTitolTextView;
        private Expedient mExpedient;

        public ExpedientHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mIdentificadorTextView = (TextView) itemView.findViewById(R.id.text_view_identificador);
            mTitolTextView = (TextView) itemView.findViewById(R.id.text_view_titol);
            mIconImageView = (ImageView) itemView.findViewById(R.id.ic_item_expedient);
            mIconImageView.setColorFilter(DefinedColors.cyan());
        }

        public void bindExpedient(Expedient expedient) {
            mExpedient = expedient;
            mIdentificadorTextView.setText(expedient.getId());
            String titol = mExpedient.getTitol();
            titol = titol.substring(0, Math.min(titol.length(), 20)) + "...";
            String text = String.format("%s - [%s] - [%s]", titol, mExpedient.getPrioritat(), mExpedient.getLiteralEstat());
            mTitolTextView.setText(text);
        }

        @Override
        public void onClick(View v) {
            Intent intent = ExpedientActivity.newIntent(getActivity(), mExpedient.getEmail(), mExpedient.getId());
            startActivity(intent);
        }
    }

    private class ExpedientAdapter extends RecyclerView.Adapter<ExpedientHolder> {
        private List<Expedient> mExpedients;

        public ExpedientAdapter(List<Expedient> expedients) {
            mExpedients = expedients;
        }

        @Override
        public ExpedientHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_list_expedient, parent, false);
            return new ExpedientHolder(view);
        }

        @Override
        public void onBindViewHolder(ExpedientHolder holder, int position) {
            Expedient expedient = mExpedients.get(position);
            holder.bindExpedient(expedient);
        }

        @Override
        public int getItemCount() {
            return mExpedients.size();
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mClient);
                        updateUI();

                        LocationRequest request = LocationRequest.create();
                        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        request.setNumUpdates(1);
                        request.setInterval(0);

                        LocationServices.FusedLocationApi.requestLocationUpdates(mClient, request, new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                                mLastLocation = location;
                                updateUI();
                            }
                        });
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .build();
        mClient.connect();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mWhereFilter = GestorPersistencia.getStringPreference(getActivity(), THIS_SCOPE, FILTRE_EXPEDIENTS, "");

        String ordreGravat = GestorPersistencia.getStringPreference(getActivity(), THIS_SCOPE, ORDRE_EXPEDIENTS, null);
        if (ordreGravat != null) {
            mOrderBy = ordreGravat;
        } else {
            setOrdreDefecte();
        }

        mEmail = getArguments().getString(ARG_EMAIL);
        if (mEmail == null) {
            mEmail = GestorPersistencia.getStringPreference(getActivity(), THIS_SCOPE, ARG_EMAIL, "");
        }

        View view = inflater.inflate(R.layout.fragment_list_expedients, container, false);

        mExpedientsRecyclerView = (RecyclerView) view.findViewById(R.id.expedients_recycler_view);
        mExpedientsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        mExpedientsRecyclerView.addItemDecoration(dividerItemDecoration);

        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        TextView emailTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_neader_expedients_text_view_email);
        ImageView imageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.ic_app_icon);
        imageView.setColorFilter(DefinedColors.white());
        emailTextView.setText(mEmail);


        setFloatingActionButton();
        updateUI();

        return view;
    }


    private void setFloatingActionButton() {
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Expedient expedient = new Expedient(mEmail, String.valueOf(DateTimeHelper.nowUnixEpoch()));
                GestorPersistencia.getInstance(getActivity()).addExpedient(expedient);
                Intent intent = ExpedientActivity.newIntent(getActivity(), expedient.getEmail(), expedient.getId());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        GestorPersistencia.setStringPreference(getActivity(), THIS_SCOPE, ARG_EMAIL, mEmail);
        GestorPersistencia.setStringPreference(getActivity(), THIS_SCOPE, ORDRE_EXPEDIENTS, mOrderBy);
        GestorPersistencia.setStringPreference(getActivity(), THIS_SCOPE, FILTRE_EXPEDIENTS, mWhereFilter);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mClient.isConnected()) {
            mClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mClient != null) {
            mClient.disconnect();
        }
    }

    public void setSearchText(String searchText) {
        mSearchText = searchText;
    }

    public void updateUI() {
        String whereClause;
        String[] whereArgs;

        if (mSearchText != null) {
            whereClause = String.format("%s = ? AND (%s LIKE ? OR %s LIKE ?)",
                    DbSchema.ExpedientTable.Cols.EMAIL, DbSchema.ExpedientTable.Cols.ID, DbSchema.ExpedientTable.Cols.TITOL);
            whereArgs = new String[]{mEmail, "%" + mSearchText + "%", "%" + mSearchText + "%"};
        } else {
            whereClause = String.format("%s = ?", DbSchema.ExpedientTable.Cols.EMAIL);
            whereArgs = new String[]{mEmail};
        }

        List<Expedient> expedients;

        switch (mOrderBy) {
            case ORDER_BY_PROXIMITAT_PRIORITAT:
                expedients = GestorPersistencia.getInstance(getActivity()).getExpedients(whereClause + " " + mWhereFilter, whereArgs, null);
                GestorOrdenacionsEspecials.ordenarPerProximitatPrioritat(expedients, mLastLocation);
                break;

            case ORDER_BY_PRIORITAT_PROXIMITAT:
                expedients = GestorPersistencia.getInstance(getActivity()).getExpedients(whereClause + " " + mWhereFilter, whereArgs, null);
                GestorOrdenacionsEspecials.ordenarPerPrioritatProximitat(expedients, mLastLocation);
                break;

            default:
                expedients = GestorPersistencia.getInstance(getActivity()).getExpedients(whereClause + " " + mWhereFilter, whereArgs, mOrderBy);
        }

        mAdapter = new ExpedientAdapter(expedients);
        mExpedientsRecyclerView.setAdapter(mAdapter);
    }

    public void setOrdreDefecte() {
        setOrdreCronologic();
    }

    public void setOrdreCronologic() {
        mOrderBy = DbSchema.ExpedientTable.Cols.ID;
    }

    public void setOrdrePrioritat() {
        mOrderBy = DbSchema.ExpedientTable.Cols.PRIORITAT + ", " + DbSchema.ExpedientTable.Cols.ID;
    }

    public void setOrdreProximitatPrioritat() {
        mOrderBy = ORDER_BY_PROXIMITAT_PRIORITAT;
    }

    public void setOrdrePrioritatProximitat() {
        mOrderBy = ORDER_BY_PRIORITAT_PROXIMITAT;
    }

    public void setFiltreGeneralOberts() {
        mWhereFilter = String.format("AND %s == '%s'", DbSchema.ExpedientTable.Cols.ESTAT, Expedient.ESTAT_OBERT);
    }

    public void setFiltreGeneralEspera() {
        mWhereFilter = String.format("AND %s == '%s'", DbSchema.ExpedientTable.Cols.ESTAT, Expedient.ESTAT_ESPERA);
    }

    public void setFiltreGeneralTancats() {
        mWhereFilter = String.format("AND %s == '%s'", DbSchema.ExpedientTable.Cols.ESTAT, Expedient.ESTAT_TANCAT);
    }

    public void setFiltreGeneralTots() {
        mWhereFilter = "";
    }

    public void fetchExpedients() {
        new FetchExpedientsTask().execute();
    }

    private class FetchExpedientsTask extends AsyncTask<Void, Void, Integer> {

        private String WS_URK = "http://radiacrom.com:8000/demo";

        private Expedient expedientFromJSON(String email, JSONObject expedient) throws org.json.JSONException {
            Expedient exp = new Expedient(email, expedient.getString("id"));

            exp.setTitol(expedient.getString("titol"));
            exp.setDescripcio(expedient.getString("descripcio"));
            exp.setEstat(expedient.getString("estat"));
            exp.setPrioritat(expedient.getInt("prioritat"));
            exp.setMomentCreacio(expedient.getInt("moment_creacio"));
            exp.setMomentTancament(expedient.getInt("moment_tancament"));

            Location loc = new Location("");
            loc.setLatitude(Double.valueOf(expedient.getString("latitud")));
            loc.setLongitude(Double.valueOf(expedient.getString("longitud")));
            exp.setLocation(loc);

            return exp;
        }

        private Nota notaFromJSON(String email, JSONObject nota) throws org.json.JSONException {
            Nota newNota = null;

            switch (nota.getInt("tipus")) {
                case Nota.NOTA_TEXT:
                    newNota = new NotaText(email, nota.getString("expedient"));
                    break;

                case Nota.NOTA_IMATGE:
                    newNota = new NotaImatge(email, nota.getString("expedient"));
                    break;

                case Nota.NOTA_AUDIO:
                    newNota = new NotaAudio(email, nota.getString("expedient"));
                    break;
            }

            newNota.setId(nota.getString("id"));
            newNota.setTitol(nota.getString("titol"));

            Location loc = new Location("");
            loc.setLatitude(Double.valueOf(nota.getString("latitud")));
            loc.setLongitude(Double.valueOf(nota.getString("longitud")));
            newNota.setLocation(loc);

            newNota.setMomentCreacio(nota.getInt("moment_creacio"));

            return newNota;
        }

        private void eliminarTotsExpedients(String email) {
            String where = "email = ?";
            String[] whereArgs = new String[] { email };
            List<Expedient> expedients = GestorPersistencia.getInstance(getActivity()).getExpedients(where, whereArgs, null);

            for (Expedient exp : expedients) {
                GestorPersistencia.getInstance(getActivity()).eliminarExpedientById(exp.getEmail(), exp.getId());
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            updateUI();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                String result = new HttpFetcher().getUrlString(WS_URK);
                Log.i("INFO", result);
                eliminarTotsExpedients(mEmail);

                JSONArray expedients = new JSONArray(result);

                for(int i = 0; i < expedients.length(); i++) {
                    JSONObject jsonExp = expedients.getJSONObject(i);

                    Expedient newExp = expedientFromJSON(mEmail, jsonExp);
                    GestorPersistencia.getInstance(getActivity()).addExpedient(newExp);

                    JSONArray jsonNotes = jsonExp.getJSONArray("notes");
                    for(int j = 0; j < expedients.length(); j++) {
                        Nota newNota = notaFromJSON(mEmail, jsonNotes.getJSONObject(j));
                        GestorPersistencia.getInstance(getActivity()).addRawNota(newNota);
                    }
                }

                return 0;
            }
            catch (JSONException ex) {
                Log.e("ERROR", "Error en parsejar el json");
            }
            catch (IOException ex) {
                Log.e("ERROR", "Error en recuperar expedients");
            }

            return null;
        }
    }
}
