package com.expnotes.persistencia;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.expnotes.model.Expedient;
import com.expnotes.model.Nota;
import com.expnotes.utils.DateTimeHelper;
import com.expnotes.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmimora on 12/11/16.
 */

public class GestorPersistencia {

    private Context mContext;
    private SQLiteDatabase mDatabase;


    public static GestorPersistencia getInstance(Context context) {
        return new GestorPersistencia(context);
    }

    private GestorPersistencia(Context context){
        mContext = context;
        mDatabase = new DbHelper(context).getWritableDatabase();
    }

    private static ContentValues getContentValuesPerAExpedient(Expedient expedient) {
        ContentValues values = new ContentValues();

        String latitud = null;
        String longitud = null;

        if (expedient.getLocation() != null) {
            latitud = String.valueOf(expedient.getLocation().getLatitude());
            longitud = String.valueOf(expedient.getLocation().getLongitude());
        }

        values.put(DbSchema.ExpedientTable.Cols.EMAIL, expedient.getEmail());
        values.put(DbSchema.ExpedientTable.Cols.ID, expedient.getId());
        values.put(DbSchema.ExpedientTable.Cols.DESCRIPCIO, expedient.getDescripcio());
        values.put(DbSchema.ExpedientTable.Cols.TITOL, expedient.getTitol());
        values.put(DbSchema.ExpedientTable.Cols.ESTAT, expedient.getEstat());
        values.put(DbSchema.ExpedientTable.Cols.PRIORITAT, expedient.getPrioritat());
        values.put(DbSchema.ExpedientTable.Cols.LATITUD, latitud);
        values.put(DbSchema.ExpedientTable.Cols.LONGITUD, longitud);
        values.put(DbSchema.ExpedientTable.Cols.MOMENT_CREACIO, expedient.getMomentCreacio());
        values.put(DbSchema.ExpedientTable.Cols.MOMENT_TANCAMENT, expedient.getMomentTancament());

        return values;
    }

    private static ContentValues getContentValuesPerANota(Nota nota) {
        ContentValues values = new ContentValues();

        String latitud = null;
        String longitud = null;

        if (nota.getLocation() != null) {
            latitud = String.valueOf(nota.getLocation().getLatitude());
            longitud = String.valueOf(nota.getLocation().getLongitude());
        }

        values.put(DbSchema.NotaTable.Cols.EMAIL, nota.getEmail());
        values.put(DbSchema.NotaTable.Cols.EXPEDIENT, nota.getExpedient());
        values.put(DbSchema.NotaTable.Cols.ID, nota.getId().toString());
        values.put(DbSchema.NotaTable.Cols.TITOL, nota.getTitol());
        values.put(DbSchema.NotaTable.Cols.LATITUD, latitud);
        values.put(DbSchema.NotaTable.Cols.LONGITUD, longitud);
        values.put(DbSchema.NotaTable.Cols.TIPUS, nota.tipusNota());
        values.put(DbSchema.NotaTable.Cols.MOMENT_CREACIO, nota.getMomentCreacio());

        return values;
    }

    public void addExpedient(Expedient expedient) {
        ContentValues values = getContentValuesPerAExpedient(expedient);
        mDatabase.insert(DbSchema.ExpedientTable.NAME, null, values);
    }

    public void updateExpedient(Expedient expedient) {
        ContentValues values = getContentValuesPerAExpedient(expedient);

        String whereClause = String.format("%s = ? AND %s = ?", DbSchema.ExpedientTable.Cols.EMAIL, DbSchema.ExpedientTable.Cols.ID);
        String[] whereArgs = new String[] { expedient.getEmail(), expedient.getId()} ;

        mDatabase.update(DbSchema.ExpedientTable.NAME, values, whereClause, whereArgs);
    }

    public void updateNota(Nota nota) {
        ContentValues values = getContentValuesPerANota(nota);

        String whereClause = String.format("%s = ? AND %s = ? AND %s = ?", DbSchema.NotaTable.Cols.EMAIL, DbSchema.NotaTable.Cols.EXPEDIENT, DbSchema.NotaTable.Cols.ID);
        String[] whereArgs = new String[] { nota.getEmail(), nota.getExpedient(), nota.getId() } ;

        mDatabase.update(DbSchema.NotaTable.NAME, values, whereClause, whereArgs);
    }

    public void eliminarExpedientById(String email, String id) {
        String whereClause = String.format("%s = ? AND %s = ?", DbSchema.ExpedientTable.Cols.EMAIL, DbSchema.ExpedientTable.Cols.ID);
        String[] whereArgs = new String[] { email, id } ;

        eliminarNotesExpedient(email, id);

        mDatabase.delete(DbSchema.ExpedientTable.NAME, whereClause, whereArgs);
    }

    private void eliminarNotesExpedient(String email, String id) {
        String whereClause = String.format("%s = ? AND %s = ?", DbSchema.NotaTable.Cols.EMAIL, DbSchema.NotaTable.Cols.EXPEDIENT);
        String[] whereArgs = new String[] { email, id } ;
        String orderBy = null;

        for(Nota nota : getNotes(whereClause, whereArgs, orderBy)) {
            deleteNotaById(email, id, nota.getId());
        }
    }

    public void addNota(Nota nota) {
        nota.setId(String.valueOf(DateTimeHelper.nowUnixEpoch()));
        nota.setMomentCreacio(DateTimeHelper.nowUnixEpoch());

        ContentValues values = getContentValuesPerANota(nota);
        mDatabase.insert(DbSchema.NotaTable.NAME, null, values);
    }

    public void addRawNota(Nota nota) {
        ContentValues values = getContentValuesPerANota(nota);
        mDatabase.insert(DbSchema.NotaTable.NAME, null, values);
    }

    private ExpedientCursorWrapper getCursorExpedients(String whereClause, String[] whereArgs, String orderBy) {
        Cursor cursor = mDatabase.query(
                                DbSchema.ExpedientTable.NAME,
                                null, // totes les columnes
                                whereClause,
                                whereArgs,
                                null, // group by
                                null, // having
                                orderBy
        );

        return new ExpedientCursorWrapper(cursor);
    }

    private NotaCursorWrapper getCursorNotes(String whereClause, String[] whereArgs, String orderBy) {
        Cursor cursor = mDatabase.query(
                DbSchema.NotaTable.NAME,
                null, // totes les columnes
                whereClause,
                whereArgs,
                null, // group by
                null, // having
                orderBy
        );

        return new NotaCursorWrapper(cursor);
    }

    public List<Expedient> getExpedients(String whereClause, String[] whereArgs, String orderBy) {
        List<Expedient> expedients = new ArrayList<Expedient>();

        ExpedientCursorWrapper cursor = getCursorExpedients(whereClause, whereArgs, orderBy);

        try {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                expedients.add(cursor.getExpedient());
                cursor.moveToNext();
            }
        }
        finally {
            cursor.close();
        }

        return expedients;
    }

    public List<Nota> getNotes(String whereClause, String[] whereArgs, String orderBy) {
        List<Nota> expedients = new ArrayList<Nota>();

        NotaCursorWrapper cursor = getCursorNotes(whereClause, whereArgs, orderBy);

        try {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                expedients.add(cursor.getNota());
                cursor.moveToNext();
            }
        }
        finally {
            cursor.close();
        }

        return expedients;
    }

    public Expedient getExpedientById(String email, String id) {
        String whereClausule = DbSchema.ExpedientTable.Cols.EMAIL + " = ? AND " + DbSchema.ExpedientTable.Cols.ID + "= ?";
        String[] whereArgs = new String[]{ email, id };
        String orderBy = null;

        List<Expedient> expedients = this.getExpedients(whereClausule, whereArgs, orderBy);

        if (expedients.size() == 0) return null;

        return expedients.get(0);
    }

    public Nota getNotaById(String email, String idExp, String idNota) {
        String whereClause = DbSchema.ExpedientTable.Cols.EMAIL + " = ? AND " + DbSchema.NotaTable.Cols.EXPEDIENT + " = ? AND " + DbSchema.NotaTable.Cols.ID + " = ?";
        String[] whereArgs = new String[]{ email, idExp, idNota.toString() } ;
        String orderBy = null;

        List<Nota> notes = this.getNotes(whereClause, whereArgs, orderBy);

        if (notes.size() == 0) return null;

        return notes.get(0);
    }

    public void deleteNotaById(String email, String idExp, String idNota) {
        String whereClause = String.format("%s = ? AND %s = ? AND %s = ?", DbSchema.NotaTable.Cols.EMAIL, DbSchema.NotaTable.Cols.EXPEDIENT, DbSchema.NotaTable.Cols.ID);
        String[] whereArgs = new String[] { email, idExp, idNota } ;

        Nota nota = getNotaById(email, idExp, idNota);
        String fitxer = nota.getNomFitxer();

        if (fitxer != null) {
            boolean esborrat = FileUtils.deleteFileInStorage(mContext, fitxer);
            Log.i("expnotes", String.format("Esborrat %s, %s", fitxer, esborrat));
        }

        mDatabase.delete(DbSchema.NotaTable.NAME, whereClause, whereArgs);
    }

    public static void setStringPreference(Activity activity, String scope, String key, String value) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(scope + "_" + key, value);
        editor.commit();
    }

    public static String getStringPreference(Activity activity, String scope, String key, String defaultValue) {
        return activity.getPreferences(Context.MODE_PRIVATE).getString(scope + "_" + key, defaultValue);
    }
}
