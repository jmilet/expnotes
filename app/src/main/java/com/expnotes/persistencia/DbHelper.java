package com.expnotes.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jmimora on 6/12/16.
 */

public class DbHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;
    public static final String DATABASE_NAME = "expnotes.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTableExpedients(db);
        createTableNotes(db);
    }

    private void createTableExpedients(SQLiteDatabase db) {
        String sql = "create table " + DbSchema.ExpedientTable.NAME  + " (" +
                DbSchema.ExpedientTable.Cols.EMAIL                   + ", " +
                DbSchema.ExpedientTable.Cols.ID                      + ", " +
                DbSchema.ExpedientTable.Cols.TITOL                   + ", " +
                DbSchema.ExpedientTable.Cols.DESCRIPCIO              + ", " +
                DbSchema.ExpedientTable.Cols.ESTAT                   + ", " +
                DbSchema.ExpedientTable.Cols.PRIORITAT               + ", " +
                DbSchema.ExpedientTable.Cols.LATITUD                 + ", " +
                DbSchema.ExpedientTable.Cols.LONGITUD                + ", " +
                DbSchema.ExpedientTable.Cols.MOMENT_CREACIO          + ", " +
                DbSchema.ExpedientTable.Cols.MOMENT_TANCAMENT        + ", " +
                "PRIMARY KEY (" + DbSchema.ExpedientTable.Cols.EMAIL + ", " + DbSchema.ExpedientTable.Cols.ID + "))";

        db.execSQL(sql);
    }

    private void createTableNotes(SQLiteDatabase db) {
        String sql = "create table " + DbSchema.NotaTable.NAME      + " (" +
                DbSchema.NotaTable.Cols.EMAIL                       + ", " +
                DbSchema.NotaTable.Cols.EXPEDIENT                   + ", " +
                DbSchema.NotaTable.Cols.ID                          + ", " +
                DbSchema.NotaTable.Cols.TITOL                       + ", " +
                DbSchema.NotaTable.Cols.LATITUD                     + ", " +
                DbSchema.NotaTable.Cols.LONGITUD                    + ", " +
                DbSchema.NotaTable.Cols.TIPUS                       + ", " +
                DbSchema.NotaTable.Cols.MOMENT_CREACIO              + ", " +
                "PRIMARY KEY (" + DbSchema.NotaTable.Cols.EMAIL + ", " + DbSchema.NotaTable.Cols.EXPEDIENT + ", " + DbSchema.NotaTable.Cols.ID + "))";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
