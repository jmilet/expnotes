package com.expnotes.persistencia;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.location.Location;

import com.expnotes.model.Expedient;

/**
 * Created by jmimora on 6/12/16.
 */

public class ExpedientCursorWrapper extends CursorWrapper {

    public ExpedientCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Expedient getExpedient() {
        String email = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.EMAIL));
        String id = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.ID));
        String titol = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.TITOL));
        String descripcio = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.DESCRIPCIO));
        String estat = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.ESTAT));
        int prioritat = getInt(getColumnIndex(DbSchema.ExpedientTable.Cols.PRIORITAT));
        String latitud = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.LATITUD));
        String longitud = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.LONGITUD));
        long momentCreacio = getLong(getColumnIndex(DbSchema.ExpedientTable.Cols.MOMENT_CREACIO));
        long momentTancament = getLong(getColumnIndex(DbSchema.ExpedientTable.Cols.MOMENT_TANCAMENT));

        Location location = null;
        if (latitud != null && longitud != null) {
            location = new Location("");
            location.setLatitude(Double.valueOf(latitud));
            location.setLongitude(Double.valueOf(longitud));
        }

        Expedient expedient = new Expedient(email, id);

        expedient.setTitol(titol);
        expedient.setDescripcio(descripcio);
        expedient.setLocation(location);
        expedient.setEstat(estat);
        expedient.setPrioritat(prioritat);
        expedient.setMomentCreacio(momentCreacio);
        expedient.setMomentTancament(momentTancament);

        return expedient;
    }
}
