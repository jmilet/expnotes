package com.expnotes.persistencia;

/**
 * Created by jmimora on 6/12/16.
 */

public class DbSchema {

    public static class ExpedientTable {
        public static final String NAME = "expedients";

        public static final class Cols {
            public static final String EMAIL = "email";
            public static final String ID = "id";
            public static final String DESCRIPCIO = "descripcio";
            public static final String TITOL = "titol";
            public static final String ESTAT = "estat";
            public static final String PRIORITAT = "prioritat";
            public static final String LONGITUD = "longitud";
            public static final String LATITUD = "latitud";
            public static final String MOMENT_CREACIO = "moment_creacio";
            public static final String MOMENT_TANCAMENT = "moment_tancament";
        }
    }

    public static class NotaTable {
        public static final String NAME = "nota";

        public static final class Cols {
            public static final String EMAIL = "email";
            public static final String EXPEDIENT = "expedient";
            public static final String ID = "id";
            public static final String TITOL = "titol";
            public static final String LONGITUD = "longitud";
            public static final String LATITUD = "latitud";
            public static final String TIPUS = "tipus";
            public static final String MOMENT_CREACIO = "moment_creacio";
        }
    }
}
