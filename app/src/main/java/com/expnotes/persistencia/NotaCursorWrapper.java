package com.expnotes.persistencia;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.location.Location;

import com.expnotes.model.Expedient;
import com.expnotes.model.Nota;

import java.util.UUID;

/**
 * Created by jmimora on 6/12/16.
 */

public class NotaCursorWrapper extends CursorWrapper {

    public NotaCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Nota getNota() {
        String email = getString(getColumnIndex(DbSchema.NotaTable.Cols.EMAIL));
        String expedient = getString(getColumnIndex(DbSchema.NotaTable.Cols.EXPEDIENT));
        String id = getString(getColumnIndex(DbSchema.NotaTable.Cols.ID));
        String titol = getString(getColumnIndex(DbSchema.NotaTable.Cols.TITOL));
        String latitud = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.LATITUD));
        String longitud = getString(getColumnIndex(DbSchema.ExpedientTable.Cols.LONGITUD));
        int tipus = getInt(getColumnIndex(DbSchema.NotaTable.Cols.TIPUS));
        long momentCreacio = getLong(getColumnIndex(DbSchema.NotaTable.Cols.MOMENT_CREACIO));

        Location location = null;
        if (latitud != null && longitud != null) {
            location = new Location("");
            location.setLatitude(Double.valueOf(latitud));
            location.setLongitude(Double.valueOf(longitud));
        }

        Nota nota = Nota.newNota(email, expedient, tipus);

        nota.setId(id);
        nota.setTitol(titol);
        nota.setLocation(location);
        nota.setMomentCreacio(momentCreacio);

        return nota;
    }
}
