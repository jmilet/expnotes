package com.expnotes.utils;

import android.graphics.Color;

/**
 * Created by jmimora on 26/11/16.
 */

public class DefinedColors {

    public static int cyan() {
        return Color.rgb(127,204,204);
    }

    public static int red() {
        return Color.rgb(255, 127, 127);
    }

    public static int gray() {
        return Color.rgb(128, 128, 128);
    }

    public static int blue() {
        return Color.rgb(127, 127, 255);
    }

    public static int white() { return Color.rgb(255, 255, 255); }

}
