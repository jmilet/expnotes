package com.expnotes.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by jmimora on 27/11/16.
 */

public class DateTimeHelper {

    public static long nowUnixEpoch() {
        return System.currentTimeMillis() / 1000L;
    }

    public static String unixEpochToString(long time) {
        Date date = new Date(time * 1000L);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        return format.format(date);
    }
}
