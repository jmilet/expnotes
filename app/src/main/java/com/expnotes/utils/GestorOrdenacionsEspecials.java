package com.expnotes.utils;

import android.location.Location;

import com.expnotes.model.Expedient;
import com.expnotes.persistencia.GestorPersistencia;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jmimora on 10/12/16.
 */

public class GestorOrdenacionsEspecials {
    public static void ordenarPerProximitatPrioritat(List<Expedient> expedients, final Location aqui) {
        Collections.sort(expedients, new Comparator<Expedient>() {
            public int compare(Expedient e1, Expedient e2) {
                if (e1.getLocation() == null || e2.getLocation() == null || aqui == null) {
                    return 1;
                }

                float d1 = e1.getLocation().distanceTo(aqui);
                float d2 = e2.getLocation().distanceTo(aqui);

                if (d1 < d2) {
                    return -1;
                } else if (d1 > d2) {
                    return 1;
                } else {
                    return e1.getPrioritat() <= e2.getPrioritat() ? -1 : 1;
                }
            }
        });
    }

    public static void ordenarPerPrioritatProximitat(List<Expedient> expedients, final Location aqui) {
        Collections.sort(expedients, new Comparator<Expedient>() {
            public int compare(Expedient e1, Expedient e2) {
                if (e1.getPrioritat() < e2.getPrioritat()) {
                    return -1;
                }
                else if (e1.getPrioritat() > e2.getPrioritat()){
                    return 1;
                }
                else {
                    if (e1.getLocation() == null || e2.getLocation() == null || aqui == null) {
                        return 1;
                    }

                    return e1.getLocation().distanceTo(aqui) <= e2.getLocation().distanceTo(aqui) ? -1 : 1;
                }
            }
        });
    }
}
