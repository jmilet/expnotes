package com.expnotes.utils;

import android.content.Context;
import android.os.Environment;

import com.expnotes.model.Nota;

import java.io.File;

/**
 * Created by jmimora on 6/12/16.
 */

public class FileUtils {

    public static File getFileInStorage(Context context, String nomFitxer) {
        File externalFileDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (externalFileDir == null) {
            return null;
        }

        return new File(externalFileDir, nomFitxer);
    }

    public static boolean deleteFileInStorage(Context context, String nomFitxer) {
        File externalFileDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (externalFileDir == null) {
            return false;
        }

        File file = new File(externalFileDir, nomFitxer);
        return file.delete();
    }

}
