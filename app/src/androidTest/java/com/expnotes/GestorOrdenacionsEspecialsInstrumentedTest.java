package com.expnotes;

import android.content.Context;
import android.location.Location;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.expnotes.model.Expedient;
import com.expnotes.persistencia.GestorPersistencia;
import com.expnotes.utils.GestorOrdenacionsEspecials;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static com.expnotes.persistencia.DbHelper.DATABASE_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GestorOrdenacionsEspecialsInstrumentedTest {

    @Before
    public void setUp() {
    }

    @Test
    public void ordreProximitatPrioritat() throws Exception {
        String email = "test@test.com";
        ArrayList<Expedient> expedients = new ArrayList<>();

        Expedient e1  = createTestExpedient(email,  "1", 31, 31, 5);
        Expedient e2  = createTestExpedient(email,  "2", 32, 32, 5);
        Expedient e3  = createTestExpedient(email,  "3", 33, 33, 5);
        Expedient e4  = createTestExpedient(email,  "4", 35, 35, 5); // <= igual proximitat, diferent prioritat
        Expedient e5  = createTestExpedient(email,  "5", 35, 35, 1); // <= igual proximitat, diferent prioritat
        Expedient e6  = createTestExpedient(email,  "6", 36, 36, 5);
        Expedient e7  = createTestExpedient(email,  "7", 37, 37, 5);
        Expedient e8  = createTestExpedient(email,  "8", 38, 38, 5);
        Expedient e9  = createTestExpedient(email,  "9", 39, 39, 5);
        Expedient e10 = createTestExpedient(email, "10", 40, 40, 5);

        expedients.add(e10);
        expedients.add(e9);
        expedients.add(e8);
        expedients.add(e7);
        expedients.add(e6);
        expedients.add(e5);
        expedients.add(e4);
        expedients.add(e3);
        expedients.add(e2);
        expedients.add(e1);

        Location aqui = new Location("aqui");
        aqui.setLatitude(20);
        aqui.setLongitude(20);

        ArrayList<Expedient> esperat = new ArrayList<>();

        esperat.add(e1);
        esperat.add(e2);
        esperat.add(e3);
        esperat.add(e5); // <= canviats.
        esperat.add(e4); // <= canviats.
        esperat.add(e6);
        esperat.add(e7);
        esperat.add(e8);
        esperat.add(e9);
        esperat.add(e10);

        GestorOrdenacionsEspecials.ordenarPerProximitatPrioritat(expedients, aqui);

        assertEquals(expedients, esperat);
    }

    @Test
    public void ordreProximitatPrioritatSenseLocation() throws Exception {
        String email = "test@test.com";
        ArrayList<Expedient> expedients = new ArrayList<>();

        Expedient e1  = createTestExpedient(email,  "1", 1, 1, 5);
        Expedient e2  = createTestExpedient(email,  "2", 2, 2, 5);

        e1.setLocation(null); // <= anulem location.
        e2.setLocation(null); // <= anulem location.

        expedients.add(e2);
        expedients.add(e1);

        Location aqui = null; // <= anulem location.

        ArrayList<Expedient> esperat = new ArrayList<>();

        esperat.add(e2); // <= res no canvia.
        esperat.add(e1);

        GestorOrdenacionsEspecials.ordenarPerProximitatPrioritat(expedients, aqui);

        assertEquals(expedients, esperat);
    }

    @Test
    public void ordrePrioritatProximitatSenseLocation() throws Exception {
        String email = "test@test.com";
        ArrayList<Expedient> expedients = new ArrayList<>();

        Expedient e1  = createTestExpedient(email,  "1", 10, 10, 2);
        Expedient e2  = createTestExpedient(email,  "2", 20, 20, 2);

        e1.setLocation(null); // <= anulem location.
        e2.setLocation(null); // <= anulem location.

        expedients.add(e2);
        expedients.add(e1);

        Location aqui = null; // <= anulem location.

        ArrayList<Expedient> esperat = new ArrayList<>();

        esperat.add(e2); // <= res no canvia.
        esperat.add(e1);

        GestorOrdenacionsEspecials.ordenarPerPrioritatProximitat(expedients, aqui);

        assertEquals(expedients, esperat);
    }

    @Test
    public void ordrePrioritatProximitat() throws Exception {
        String email = "test@test.com";
        ArrayList<Expedient> expedients = new ArrayList<>();

        Expedient e1  = createTestExpedient(email,  "1", 30, 30, 1);
        Expedient e2  = createTestExpedient(email,  "2", 31, 31, 2);
        Expedient e3  = createTestExpedient(email,  "3", 32, 32, 3);
        Expedient e4  = createTestExpedient(email,  "4", 33, 33, 4);
        Expedient e5  = createTestExpedient(email,  "5", 39, 39, 5); // <= igual prioritat, però cada vegada més pròxim.
        Expedient e6  = createTestExpedient(email,  "6", 38, 38, 5);
        Expedient e7  = createTestExpedient(email,  "7", 37, 37, 5);
        Expedient e8  = createTestExpedient(email,  "8", 36, 36, 5);
        Expedient e9  = createTestExpedient(email,  "9", 35, 35, 5);
        Expedient e10 = createTestExpedient(email, "10", 34, 34, 5);

        expedients.add(e10);
        expedients.add(e9);
        expedients.add(e8);
        expedients.add(e7);
        expedients.add(e6);
        expedients.add(e5);
        expedients.add(e4);
        expedients.add(e3);
        expedients.add(e2);
        expedients.add(e1);

        Location aqui = new Location("aqui");
        aqui.setLatitude(20);
        aqui.setLongitude(20);

        ArrayList<Expedient> esperat = new ArrayList<>();

        esperat.add(e1);
        esperat.add(e2);
        esperat.add(e3);
        esperat.add(e4);
        esperat.add(e10);
        esperat.add(e9);
        esperat.add(e8);
        esperat.add(e7);
        esperat.add(e6);
        esperat.add(e5);

        GestorOrdenacionsEspecials.ordenarPerPrioritatProximitat(expedients, aqui);

        assertEquals(expedients, esperat);
    }

    private Expedient createTestExpedient(String email, String id, double latitud, double longitud, int prioritat) {
        Expedient exp = new Expedient(email, id);
        exp.setLocation(new Location(""));
        exp.getLocation().setLatitude(latitud);
        exp.getLocation().setLongitude(longitud);
        exp.setPrioritat(prioritat);
        return exp;
    }
}
