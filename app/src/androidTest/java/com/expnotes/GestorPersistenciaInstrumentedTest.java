package com.expnotes;

import android.content.Context;
import android.location.Location;
import android.opengl.GLException;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.expnotes.model.Expedient;
import com.expnotes.persistencia.GestorPersistencia;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.expnotes.persistencia.DbHelper.DATABASE_NAME;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GestorPersistenciaInstrumentedTest {

    @Before
    public void setUp() {
        InstrumentationRegistry.getTargetContext().deleteDatabase(DATABASE_NAME);
    }

    @Test
    public void creaExpedient() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        // Creació expedient.
        String email = "email_test";
        String identificador = "identificador_test";

        Location location = new Location("test");
        location.setLatitude(1.2);
        location.setLongitude(2.3);

        Expedient expedient = new Expedient(email, identificador);

        expedient.setDescripcio("Descripció test");
        expedient.setLocation(location);
        expedient.setEstat("ABC");

        // Recupera i compara.
        GestorPersistencia.getInstance(appContext).addExpedient(expedient);
        Expedient newExpedient = GestorPersistencia.getInstance(appContext).getExpedientById(email, identificador);

        assertEquals(expedient.getEmail(), newExpedient.getEmail());
        assertEquals(expedient.getId(), newExpedient.getId());
        assertEquals(expedient.getDescripcio(), newExpedient.getDescripcio());
        assertEquals(expedient.getEstat(), newExpedient.getEstat());
        assertEquals(expedient.getPrioritat(), newExpedient.getPrioritat());
        assertEquals(expedient.getMomentCreacio(), newExpedient.getMomentCreacio());
        assertEquals(expedient.getMomentTancament(), newExpedient.getMomentTancament());
        assertEquals(expedient.getLocation().getLatitude(), newExpedient.getLocation().getLatitude(), 0.1);
        assertEquals(expedient.getLocation().getLongitude(), newExpedient.getLocation().getLongitude(), 0.1);
        assertTrue(expedient.getMomentCreacio() > 0);
        assertTrue(expedient.getMomentTancament() == -1);
    }

    @Test
    public void sborraExpedient() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        // Creació expedient.
        String email = "email_test";
        String identificador = "identificador_test";

        Location location = new Location("test");
        location.setLatitude(1.2);
        location.setLongitude(2.3);

        Expedient expedient = new Expedient(email, identificador);

        expedient.setDescripcio("Descripció test");
        expedient.setLocation(location);
        expedient.setEstat("ABC");

        GestorPersistencia.getInstance(appContext).addExpedient(expedient);

        // Exborra i compara.
        GestorPersistencia.getInstance(appContext).eliminarExpedientById(email, identificador);

        assertTrue(GestorPersistencia.getInstance(appContext).getExpedientById(email, identificador) == null);
    }

    @Test
    public void modificaExpedient() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        // Creació expedient.
        String email = "email_test";
        String identificador = "identificador_test";

        Location location = new Location("test");
        location.setLatitude(1.2);
        location.setLongitude(2.3);

        Expedient expedient = new Expedient(email, identificador);

        expedient.setDescripcio("Descripció test");
        expedient.setLocation(location);
        expedient.setEstat(Expedient.ESTAT_OBERT);

        GestorPersistencia.getInstance(appContext).addExpedient(expedient);

        // Modifica.
        String newDescripcio = "nova descripció";

        Location newLocation = new Location("test");
        double newLatitud = 5.6;
        double newLongitud = 7.8;
        newLocation.setLatitude(newLatitud);
        newLocation.setLongitude(newLongitud);
        String newEstat = Expedient.ESTAT_ESPERA;
        int newPrioritat = 9;

        expedient.setDescripcio(newDescripcio);
        expedient.setLocation(newLocation);
        expedient.setEstat(newEstat);
        expedient.setPrioritat(newPrioritat);

        GestorPersistencia.getInstance(appContext).updateExpedient(expedient);

        // Compara.
        Expedient newExpedient = GestorPersistencia.getInstance(appContext).getExpedientById(email, identificador);

        assertEquals(newExpedient.getEmail(), newExpedient.getEmail());
        assertEquals(newExpedient.getId(), newExpedient.getId());
        assertEquals(newExpedient.getDescripcio(), newDescripcio);
        assertEquals(newExpedient.getEstat(), newEstat);
        assertEquals(newExpedient.getPrioritat(), newPrioritat);
        assertEquals(newExpedient.getMomentCreacio(), newExpedient.getMomentCreacio());
        assertEquals(newExpedient.getMomentTancament(), newExpedient.getMomentTancament());
        assertEquals(newExpedient.getLocation().getLatitude(), newLatitud, 0.1);
        assertEquals(newExpedient.getLocation().getLongitude(), newLongitud, 0.1);
        assertEquals(newExpedient.getMomentCreacio(), newExpedient.getMomentCreacio());
        assertEquals(newExpedient.getMomentTancament(), newExpedient.getMomentTancament());
    }

    @Test
    public void canviaEstatExpedient() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        // Creació expedient.
        String email = "email_test";
        String identificador = "identificador_test";

        Expedient expedient = new Expedient(email, identificador);
        GestorPersistencia.getInstance(appContext).addExpedient(expedient);

        // Tancat.
        expedient.tancar();
        GestorPersistencia.getInstance(appContext).updateExpedient(expedient);

        // Compara.
        Expedient newExpedient = GestorPersistencia.getInstance(appContext).getExpedientById(email, identificador);

        assertEquals(newExpedient.getEstat(), Expedient.ESTAT_TANCAT);
        assertTrue(newExpedient.getMomentTancament() != -1);

        // Obert.
        expedient.en_espera();
        GestorPersistencia.getInstance(appContext).updateExpedient(expedient);

        // Compara.
        newExpedient = GestorPersistencia.getInstance(appContext).getExpedientById(email, identificador);

        assertEquals(newExpedient.getEstat(), Expedient.ESTAT_ESPERA);
        assertTrue(newExpedient.getMomentTancament() == -1);

        // En espera.
        expedient.en_espera();
        GestorPersistencia.getInstance(appContext).updateExpedient(expedient);

        // Compara.
        newExpedient = GestorPersistencia.getInstance(appContext).getExpedientById(email, identificador);

        assertEquals(newExpedient.getEstat(), Expedient.ESTAT_ESPERA);
        assertTrue(newExpedient.getMomentTancament() == -1);
    }
}
